# ssr-app
[Cloud Run QuickStart - Docker to Serverless by Fireship](https://www.youtube.com/watch?v=3OP-q55hOUI)

> My nuxt app on Cloud Run &amp; Firebase

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
